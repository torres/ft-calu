#include "ft_calu.h"

int ft_calu(fttslu_data *ftd,MPI_data *mpid,int M,int N,int l_c,char *in,int argc,char **argv){
	char msg[MPI_MAX_ERROR_STRING*10];
	char err[MPI_MAX_ERROR_STRING];
	int ret=0,err_len;
	int flag;

	int i;
	int ok=1;
	int check;
	MPI_Win window;
//	MPI_Win_create(&check,sizeof(int),sizeof(int),MPI_INFO_NULL,mpid->world,&window);
//	MPI_Win_fence(0,window);

	time_calu=MPI_Wtime();
	while(currCol<colN-1){
		if(colNum==currCol)
			printf("Proceso [%d] en [%s]: grid:[%d,%d] - Procesando fila/columna[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
		switch(stage){
			case STAGE_COMM_DIVISION:
//------------------------------------------------------------PRUEBA DE ERROR------------------------------------------------------------
/*				srand(time(NULL));
				if(rand()%mpid->world_size+1==mpid->world_rank){
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Fallando...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
					raise(SIGKILL);
				}*/
//----------------------------------------------------------FIN PRUEBA DE ERROR----------------------------------------------------------

//				SI ME CORRESPONDE CALCULAR UN PANEL
				if(colNum==currCol){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Dividiendo comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					DIVIDE COMUNICADOR EXCLUYENDO PROCESOS QUE YA COMPLETARON SU CALCULO
					time_comm=MPI_Wtime();
					ret=MPI_Comm_split(col_comm,(rowNum>=currRow),rowNum,&panel_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando division de comunicador de columnas[%d]...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al dividir comunicador de columnas[%d]",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
			break;
			case STAGE_PANEL_GENERATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Generando panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					GENERA PANEL A PROCESAR CON FT-TSLU
					panelM=M-currRow*md_local->M;
					panel=get_panel(md_local,panelM,panelN,&panel_comm);
					flag=(panel!=NULL);
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando generacion de panel...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
					flag=(ret==MPI_SUCCESS);
				}
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al generar panel",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSLU_INITIALIZATION:
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Panel generado. Iniciando FT-TSLU\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					INCIALIZA AMBIENTE PARA EJECUTAR FT-TSLU
					mpid_col=MPI_data_init_with_comm(panel_comm,&argc,&argv);
					ftd->mpid=mpid_col;
					ret=ft_tslu_init(ftd,panel,panelM,panelN,l_c,in,argc,argv);
				}
//				else if(colNum==currCol && rowNum<currRow){
//					LIBERA EL COMUNICADOR TEMPORAL
//					MPI_Comm_free(&panel_comm);
//					ret=MPI_SUCCESS;
//				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando inicializacion FT-TSLU...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error de inicializacion FT-TSLU",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_FTTSLU_EXECUTION:
				check=0;
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - FT-TSLU sobre panel\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
//					print_matrix(panel,panelM,panelN);
#endif /* _VERB_ */
//					EJECUCION DE FT-TSLU SOBRE EL PANEL
					ret=ft_tslu(ftd,l_c);
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - FT-TSLU sobre panel finalizado[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,ret);
#endif /* _VERB_ */
/*					for(i=0;i<mpid->world_size;i++){
						if(i==mpid->world_rank)
							continue;
						MPI_Put(&ok,1,MPI_INT,i,0,1,MPI_INT,window);
					}
					MPI_Win_fence(0,window);*/
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando fin FT-TSLU en row_comm[%d]\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					MPI_Win_fence(0,window);
					ret=MPI_SUCCESS;
				}
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al ejecutar FT-TSLU",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_SEND_RECEIVE_L:
				time_exec_tm=MPI_Wtime();
				if(colNum==currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Distribuyendo subbloque L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					RESTAURA FILAS INICIALES DE LA MATRIZ L (SE ALMACENA EN A_init PARA AHORRAR MEMORIA)
//					swap_sub_block(ftd->md->A_init,ftd->md->M,0,ftd->md->M,ftd->md->N,ftd->md->IPIV,-1);
					MPI_Comm_rank(panel_comm,&panelRowNum);
					panelIndex=get_block_index(md_local->M,md_local->N,ftd->md->M,ftd->md->N,panelRowNum);
					get_sub_block(ftd->md->A_init,ftd->md->M,panelIndex,md_local->A,md_local->M,md_local->N);
//					DISTRIBUYE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE LINEAS
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(md_local->A,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local,mpid,rowNum,colNum,"FTCALU");
//					LIBERA EL COMUNICADOR TEMPORAL
//					MPI_Comm_free(&panel_comm);
				}
				else if(colNum>currCol && rowNum>=currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque L de proceso[%d] en row_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE MI LINEA
					sub=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(sub,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque L de proceso[%d] en row_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currCol);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE L RESULTANTE DE FT-TSLU SOBRE MI LINEA
					sub=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(sub,md_local->M*md_local->N,MPI_DOUBLE,currCol,row_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
					free(sub);
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al %s subbloque L de proceso[%d] en row_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum==currCol && rowNum>=currRow) ? "distribuir" : (colNum>currCol && rowNum>=currRow) ? "recibir" : "distribuir/recibir",currCol);
			break;
			case STAGE_SEND_RECEIVE_U:
				time_exec_tm=MPI_Wtime();
				if(colNum>currCol && rowNum==currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Distribuyendo subbloque U de proceso[%d] en col_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
//					print_matrix(md_local->A,md_local->M,md_local->N);
#endif /* _VERB_ */
//					SI ME CORRESPONDE ACTUALIZAR EL PANEL SUPERIOR DE U's CALCULA EL SUBBLOQUE U FINAL DE LA LINEA
					solve_matrix(sub,md_local->A,md_local->M,md_local->N);
					free(md_local->A);
					md_local->A=sub;
//					DISTRIBUYE SUBBLOQUE U RESULTANTE SOBRE COLUMNAS
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(md_local->A,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local,mpid,rowNum,colNum,"FTCALU");
				}
//				SI ME CORRESPONDE ACTUALIZAR EL RESTO DE LA TRAILING MATRIX
				else if(colNum>currCol && rowNum>currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque U de proceso[%d] en col_comm\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE U RESULTANTE SOBRE MI COLUMNA
					subR=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(subR,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Recibiendo subbloque U de proceso[%d] en col_comm para descartar\n",mpid->world_rank,mpid->processor_name,rowNum,colNum,currRow);
#endif /* _VERB_ */
//					RECIBE SUBBLOQUE U RESULTANTE SOBRE MI COLUMNA
					subR=(double *)malloc(md_local->M*md_local->N*sizeof(double));
					time_comm=MPI_Wtime();
					ret=MPI_Bcast(subR,md_local->M*md_local->N,MPI_DOUBLE,currRow,col_comm);
					time_comm_final+=MPI_Wtime()-time_comm;
					free(subR);
					ret=MPI_SUCCESS;
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al %s subbloque U de proceso[%d] en col_comm",mpid->world_rank,mpid->processor_name,rowNum,colNum,(colNum>currCol && rowNum==currRow) ? "distribuir" : (colNum>currCol && rowNum>currRow) ? "recibir" : "distribuir/recibir",currRow);
			break;
			case STAGE_UPDATE_TM:
				time_exec_tm=MPI_Wtime();
//				SI ME CORRESPONDE ACTUALIZAR EL RESTO DE LA TRAILING MATRIX
				if(colNum>currCol && rowNum>currRow){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Actualizando trailing matrix\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
//					CALCULA EL SUBBLOQUE A ACTUALIZADO DE LA LINEA
					temp=(double *)calloc(md_local->M*md_local->N,sizeof(double));
//					MULTIPLICA EL SUBBLOQUE DE LA TRAILING MATRIX
					mult_matrix(sub,md_local->M,md_local->N,subR,md_local->M,md_local->N,temp,0);
//					RESTA AL SUBBLOQUE A EL RESULTADO DE LA ULTIMA MULTIPLICACION
					add_matrix(md_local->A,temp,md_local->M,md_local->N,'S');
					free(md_local->A);
					md_local->A=temp;
					free(subR);
					free(sub);
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local,mpid,rowNum,colNum,"FTCALU");
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando actualizacion trailing matrix...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al actualizar la trailing matrix",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
			case STAGE_LAST_BLOCK:
				time_exec_tm=MPI_Wtime();
				if(rowNum==rowM && colNum==colN && rowNum==currRow+1 && colNum==currCol+1){
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Procesando ultimo bloque\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
					lu_matrix(md_local->A,md_local->M,md_local->N,md_local->IPIV);
//					RESPALDA EL RESULTADO EN EL ARCHIVO LOCAL
					write_subblock(md_local,mpid,rowNum,colNum,"FTCALU");
				}
				else{
#ifdef _VERB_
					usleep(WAIT_TIME*mpid->world_rank);
					printf("Proceso [%d] en [%s]: grid:[%d,%d] - Esperando ultimo bloque...\n",mpid->world_rank,mpid->processor_name,rowNum,colNum);
#endif /* _VERB_ */
				}
				time_exec_tm_final+=MPI_Wtime()-time_exec_tm;
				ret=MPI_SUCCESS;
				flag=(ret==MPI_SUCCESS);
				sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al procesar ultimo subbloque",mpid->world_rank,mpid->processor_name,rowNum,colNum);
			break;
		}
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
		MPIX_Comm_agree(mpid->world,&flag);
//		SI HAY PROCESO FALLIDOS LOS RESTAURA
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			verify_error(mpid,&cart_2D,&col_comm,&row_comm,&rowM,&colN,&rowNum,&colNum,currRow);
			continue;
		}
#else
		MPI_Barrier(mpid->world);
		if(!flag){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"%s[%d][%s]\n",msg,ret,err);
			free_timers(mpid->world_size);
//			free_fttslu_data(ftd);
			MPI_Comm_free(&col_comm);
			MPI_Comm_free(&row_comm);
			MPI_Comm_free(&cart_2D);
			MPI_Finalize();
			return(ret);
		}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
		if(stage==STAGE_LAST_BLOCK){
			currRow++;
			currCol++;
		}
		stage=next_stage(stage);
	}
	time_calu_final+=MPI_Wtime()-time_calu;
//	MPI_Win_free(&window);
	return(MPI_SUCCESS);
}

double *get_panel(matrix_data *md,int panelM,int panelN,MPI_Comm *panel_comm){
	int i,j,ret;
	int panel_comm_size,panel_comm_rank,panelIndex;
	double *panel=(double *)malloc(panelM*panelN*sizeof(double));
	double *sub=(double *)malloc(md->M*md->N*sizeof(double));
//	DISTRIBUYE SUBBLOQUES A PROCESAR CON FT-TSLU
	MPI_Comm_size(*panel_comm,&panel_comm_size);
	MPI_Comm_rank(*panel_comm,&panel_comm_rank);
	for(i=0;i<panel_comm_size;i++){
		time_comm=MPI_Wtime();
		ret=MPI_Bcast((i==panel_comm_rank) ? md->A : sub,md->M*md->N,MPI_DOUBLE,i,*panel_comm);
		time_comm_final+=MPI_Wtime()-time_comm;
		if(ret!=MPI_SUCCESS)
			return(NULL);
		MPI_Barrier(*panel_comm);
		panelIndex=get_block_index(md->M,md->N,panelM,panelN,i);
		set_sub_block(panel,panelM,panelIndex,(i==panel_comm_rank) ? md->A : sub,md->M,md->N);
	}
	free(sub);
	return(panel);
}

int create_grid_process(MPI_data *mpid,MPI_Comm *c_2D,MPI_Comm *r_comm,MPI_Comm *c_comm,int *rowM,int *colN,int *rowNum,int *colNum){
	MPI_Comm new_cart_2D;
	MPI_Comm new_col_comm;
	MPI_Comm new_row_comm;
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len,flag;
	int dims[2],periods[2],remain_dims_row[2],remain_dims_col[2];
	dims[0]=dims[1]=(int)sqrt(mpid->world_size);
	periods[0]=0;
	periods[1]=1;
	remain_dims_row[0]=1;
	remain_dims_row[1]=0;
	remain_dims_col[0]=0;
	remain_dims_col[1]=1;
	ret=MPI_Cart_create(mpid->world,2,dims,periods,0,&new_cart_2D);
	MPI_Comm_set_errhandler(new_cart_2D,MPI_ERRORS_RETURN);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(new_cart_2D,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear topologia cartesiana[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
//		MPIX_Comm_revoke(new_cart_2D);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#else
	MPI_Barrier(new_cart_2D);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear topologia cartesiana[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
	ret=MPI_Cart_sub(new_cart_2D,remain_dims_row,&new_row_comm);
	MPI_Comm_set_errhandler(new_row_comm,MPI_ERRORS_RETURN);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(new_row_comm,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de filas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
//		MPIX_Comm_revoke(new_row_comm);
//		MPIX_Comm_revoke(new_cart_2D);
		MPI_Comm_free(&new_row_comm);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#else
	MPI_Barrier(new_row_comm);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de filas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		MPI_Comm_free(&new_row_comm);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
	ret=MPI_Cart_sub(new_cart_2D,remain_dims_col,&new_col_comm);
	MPI_Comm_set_errhandler(new_col_comm,MPI_ERRORS_RETURN);
	flag=(ret==MPI_SUCCESS);
//------------------------------------------------------------VERIFICACION DE ERROR------------------------------------------------------------
#ifndef _OMPI_
	MPIX_Comm_agree(new_row_comm,&flag);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de columnas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
//		MPIX_Comm_revoke(new_col_comm);
//		MPIX_Comm_revoke(new_row_comm);
//		MPIX_Comm_revoke(new_cart_2D);
		MPI_Comm_free(&new_col_comm);
		MPI_Comm_free(&new_row_comm);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#else
	MPI_Barrier(new_row_comm);
	if(!flag){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al crear comunicador de columnas[%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		MPI_Comm_free(&new_col_comm);
		MPI_Comm_free(&new_row_comm);
		MPI_Comm_free(&new_cart_2D);
		return(ret);
	}
#endif /* _OMPI_ */
//----------------------------------------------------------FIN VERIFICACION DE ERROR----------------------------------------------------------
//	MPI_Comm_set_errhandler(*cart_2D,MPI_ERRORS_RETURN);
//	MPI_Comm_set_errhandler(*col_comm,MPI_ERRORS_RETURN);
//	MPI_Comm_set_errhandler(*row_comm,MPI_ERRORS_RETURN);
	*c_2D=new_cart_2D;
	*c_comm=new_col_comm;
	*r_comm=new_row_comm;
//	MPI_Comm_free(cart_2D);
//	MPI_Comm_free(col_comm);
//	MPI_Comm_free(row_comm);
	MPI_Comm_size(*r_comm,rowM);
	MPI_Comm_rank(*r_comm,rowNum);
	MPI_Comm_size(*c_comm,colN);
	MPI_Comm_rank(*c_comm,colNum);
	return(MPI_SUCCESS);
}

int read_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char l_c[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(l_c,"%s",(l_calc) ? "LU" : "U");
	sprintf(filename,"[%s][%s][%s][%dx%d][%dx%d][%d]_Process[%d]_Backup.dump",option,compilation,l_c,md->M,md->N,row,col,mpid->world_size,mpid->world_rank);
	FILE *in=fopen(filename,"r");
	if(in==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_read=MPI_Wtime();
		fread(md->A,sizeof(double),md->M*md->N,in);
		time_read_final+=MPI_Wtime()-time_read;
		fclose(in);
	}
	return(MPI_SUCCESS);
}

int write_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char l_c[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(l_c,"%s",(l_calc) ? "LU" : "U");
	sprintf(filename,"[%s][%s][%s][%dx%d][%dx%d][%d]_Process[%d]_Backup.dump",option,compilation,l_c,md->M,md->N,row,col,mpid->world_size,mpid->world_rank);
	FILE *out=fopen(filename,"w");
	if(out==NULL){
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
		return(EXIT_FAILURE);
	}
	else{
		time_write=MPI_Wtime();
		fwrite(md->A,sizeof(double),md->M*md->N,out);
		time_write_final+=MPI_Wtime()-time_write;
		fclose(out);
	}
	return(MPI_SUCCESS);
}

int next_stage(int curr_s){
	return((curr_s+1)%8);
}

#ifndef _OMPI_
/*int *working_list(int curr,int rowM,int colN,int *size){
	int i;
	int *working=(int *)calloc(rowM-curr,sizeof(int));
	working[0]=curr*colN+curr;
	for(i=1;i<rowM-curr;i++)
		working[i]=working[i-1]+colN;
	*size=rowM-curr;
	return(working);
}

int still_working(int *dead_rank_list,int dead_list_size,int *work_rank_list,int *work_list_size){
	int i,j;
	for(i=0;i<dead_list_size;i++){
		for(j=0;j<work_list_size;j++){
			if(dead_rank_list[i]==work_rank_list[j])
				return(1);
		}
	}
	return(0);
}*/

void verify_error(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *col_comm,MPI_Comm *row_comm,int *rowM,int *colN,int *rowNum,int *colNum,int curr){
	char msg[MPI_MAX_ERROR_STRING];
	char err[MPI_MAX_ERROR_STRING];
	int ret,err_len,i;
	int flag;
	int indexS;
	int dead_list_size;
	int surv_list_size;
	int *dead_rank_list;
	int *surv_rank_list;
	time_rest=MPI_Wtime();
	while(1){
//		CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
		dead_rank_list=dead_list(mpid,&dead_list_size);
//		CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
		surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
#ifdef _VERB_
		usleep(WAIT_TIME*mpid->world_rank);
		sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d]: Procesos fallidos: ",mpid->world_rank,mpid->processor_name,*rowNum,*colNum);
		for(i=0;i<dead_list_size;i++)
			sprintf(msg,"%s[%d]",msg,dead_rank_list[i]);
		printf("%s\n",msg);
		sprintf(msg,"Proceso [%d] en [%s]: grid:[%d,%d]: Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name,*rowNum,*colNum);
		for(i=0;i<surv_list_size;i++)
			sprintf(msg,"%s[%d]",msg,surv_rank_list[i]);
		usleep(WAIT_TIME*mpid->world_rank);
		printf("%s\n",msg);
#endif /* _VERB_ */
//		RESTAURA A LOS PROCESOS FALLIDOS
		ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//		SI LA RESTAURACION FALLO, INTENTA DE NUEVO
		if(ret!=MPI_SUCCESS){
			free(dead_rank_list);
			free(surv_rank_list);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,ret,err);
			continue;
		}
//		SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
		indexS=0;
		while(mpid->world_rank!=surv_rank_list[indexS]){
			indexS++;
		}
//		SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
		if(indexS<dead_list_size){
			for(i=indexS;i<dead_list_size;i+=surv_list_size){
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: grid:[%d,%d] - Enviando fila/columna actual [%d] a [%d]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,curr,dead_rank_list[i]);
#endif /* _VERB_ */
				ret=safe_send_times((void *)(&curr),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//				SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
				if(ret!=MPI_SUCCESS){
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al distribuir fila/columna actual [%d] a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,curr,dead_rank_list[i],ret,err);
					continue;
				}
#ifdef _VERB_
				usleep(WAIT_TIME*mpid->world_rank);
				printf("Proceso [%d] en [%s]: grid:[%d,%d] - Enviando etapa actual [%d] a [%d]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,stage,dead_rank_list[i]);
#endif /* _VERB_ */
				ret=safe_send_times((void *)(&stage),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//				SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
				if(ret!=MPI_SUCCESS){
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Error al distribuir etapa actual [%d] a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,stage,dead_rank_list[i],ret,err);
					continue;
				}
				printf("Proceso [%d] en [%s]: grid:[%d,%d] - Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,dead_rank_list[i]);
			}
		}
		ret=create_grid_process(mpid,cart_2D,col_comm,row_comm,rowM,colN,rowNum,colNum);
//		SI LA RESTAURACION FALLO, INTENTA DE NUEVO
		if(ret!=MPI_SUCCESS){
			free(dead_rank_list);
			free(surv_rank_list);
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: grid:[%d,%d] - Reintentando... [%d][%s]\n",mpid->world_rank,mpid->processor_name,*rowNum,*colNum,ret,err);
			continue;
		}
		break;
	}
	time_rest_final+=MPI_Wtime()-time_rest;
}
#endif /* _OMPI_ */

