#ifndef _FT_CALU_H_
#define _FT_CALU_H_

#include "ft_tslu.h"

#define STAGE_COMM_DIVISION				0
#define STAGE_PANEL_GENERATION			1
#define STAGE_FTTSLU_INITIALIZATION		2
#define STAGE_FTTSLU_EXECUTION			3
#define STAGE_SEND_RECEIVE_L			4
#define STAGE_SEND_RECEIVE_U			5
#define STAGE_UPDATE_TM					6
#define STAGE_LAST_BLOCK				7

int ft_calu(fttslu_data *ftd,MPI_data *mpid,int M,int N,int l_c,char *in,int argc,char **argv);
double *get_panel(matrix_data *md,int panelM,int panelN,MPI_Comm *panel_comm);
int create_grid_process(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *row_comm,MPI_Comm *col_comm,int *rowM,int *colN,int *rowNum,int *colNum);
int read_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option);
int write_subblock(matrix_data *md,MPI_data *mpid,int row,int col,char *option);
int next_stage(int curr_s);

#ifndef _OMPI_
//int *working_list(int curr,int rowM,int colN,int *size);
//int still_working(int *dead_rank_list,int dead_list_size,int *work_rank_list,int *work_list_size);
void verify_error(MPI_data *mpid,MPI_Comm *cart_2D,MPI_Comm *col_comm,MPI_Comm *row_comm,int *rowM,int *colN,int *rowNum,int *colNum,int curr);
#endif /* _OMPI_ */

MPI_data *mpid_col;
matrix_data *md_local;

MPI_Comm cart_2D;
MPI_Comm row_comm;
MPI_Comm col_comm;
MPI_Comm panel_comm;

int stage;

int rowM;
int colN;
int rowNum;
int colNum;
int currCol;
int currRow;

int panelRowNum;
int panelIndex;
int panelM;
int panelN;

double *panel;
double *sub;
double *subR;
double *temp;

#endif /* _FT_CALU_H_ */

