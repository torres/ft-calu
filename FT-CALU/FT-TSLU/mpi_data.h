#ifndef _MPI_DATA_H_
#define _MPI_DATA_H_

#include "util.h"

#define MAX_TRY	5

typedef struct _MPI_data{
	int argc;
	char **argv;
	int processor_name_len;
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int spawned;
	MPI_Comm world;
	int world_rank;
	int world_size;
	MPI_Status status;
	int step;
	int step_finished;
	int *dest;
	int *mirror;
}MPI_data;

MPI_data *MPI_data_init(int *argc,char **argv[]);
MPI_data *MPI_data_init_with_comm(MPI_Comm comm,int *argc,char **argv[]);
void MPI_data_reset(MPI_data *mpid);
void MPI_data_free(MPI_data *mpid);
void add_mirror(MPI_data *mpid);
void update_dest(MPI_data *mpid);
int find_incomplete(MPI_data *mpid);
int safe_send(void *send,int size,MPI_Datatype type,int dest,MPI_Comm world);
int safe_send_times(void *send,int size,MPI_Datatype type,int dest,MPI_Comm world,int times);
int safe_receive(void *recv,int size,MPI_Datatype type,int dest,MPI_Comm world,MPI_Status status);
int safe_receive_times(void *recv,int size,MPI_Datatype type,int dest,MPI_Comm world,MPI_Status status,int times);
int send_receive(double *send,double *recv,int M,int N,int *IPIVsend,int *IPIVrecv,MPI_data *mpid);
int send_receive_matrix(double *send,double *recv,int M,int N,MPI_data *mpid);

#ifndef _OMPI_
int replace_partners(MPI_data *mpid,int *dead_list,int dead_list_size);
int *dead_list(MPI_data *mpid,int *size);
int *survivor_list(MPI_data *mpid,int *dead_list,int dead_list_size,int *size);
#endif /* _OMPI_ */

//void init_datatype(int n);

#endif /* _MPI_DATA_H_ */

