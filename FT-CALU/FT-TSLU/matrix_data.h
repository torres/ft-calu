#ifndef _MATRIX_DATA_H_
#define _MATRIX_DATA_H_

#include "lapack.h"

typedef struct _matrix_data{
	int M;
	int N;
	double *A;
	double *A_init;
	int LDA;
	int *IPIV;
	int INFO;
	double *U;
	int Mb;
	int Nb;
}matrix_data;

matrix_data *matrix_data_init_with_random(int M,int N,int Mb,int Nb,int rank,int copy);
matrix_data *matrix_data_init_with_file(int M,int N,int Mb,int Nb,char *file,int copy);
matrix_data *matrix_data_init_with_matrix(int M,int N,int Mb,int Nb,double *A,int copy);
matrix_data *matrix_data_init(int M,int N,int Mb,int Nb,int copy);
void matrix_data_free(matrix_data *md);

void set_U(matrix_data *md);
void set_A(matrix_data *md,int src);
void set_A_init(matrix_data *md);
void print_matrix_data(matrix_data *md);

#endif /* _MATRIX_DATA_H_ */

