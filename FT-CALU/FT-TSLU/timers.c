#include "timers.h"

void init_timers(int size){
	int i;
	time_exec_final=0.0;
	time_tslu_final=0.0;
	time_mult_final=0.0;
	time_solv_final=0.0;
	time_inv_final=0.0;
	time_add_final=0.0;
	time_swap_final=0.0;
	time_rest_final=0.0;
	time_comm_final=0.0;
	time_copy_final=0.0;
	time_steps=(double *)calloc(size,sizeof(double));
	memset(time_steps,0,size*sizeof(double));
	time_calu_final=0.0;
	time_exec_tm_final=0.0;
	time_read_final=0.0;
	time_write_final=0.0;
#ifdef _TAU_
	TAU_time_steps=(char **)calloc(size,sizeof(char*));
	for(i=0;i<size;i++)
		TAU_time_steps[i]=(char *)calloc(MAX_MSG_SIZE,sizeof(char));
#endif /* _TAU_ */
}

void free_timers(int size){
	int i;
	free(time_steps);
#ifdef _TAU_
	for(i=0;i<size;i++)
		free(TAU_time_steps[i]);
	free(TAU_time_steps);
#endif /* _TAU_ */
}

