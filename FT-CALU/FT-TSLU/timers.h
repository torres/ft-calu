#ifndef _TIMERS_H_
#define _TIMERS_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpi.h>
#include <mpi-ext.h>
#include <unistd.h>
#include <signal.h>

#define MAX_MSG_SIZE	2048

#ifdef _TAU_
#include "TAU.h"
char TAU_time_exec[MAX_MSG_SIZE];
char TAU_time_tslu[MAX_MSG_SIZE];
char TAU_time_mult[MAX_MSG_SIZE];
char TAU_time_solv[MAX_MSG_SIZE];
char TAU_time_inv[MAX_MSG_SIZE];
char TAU_time_add[MAX_MSG_SIZE];
char TAU_time_swap[MAX_MSG_SIZE];
char TAU_time_rest[MAX_MSG_SIZE];
char TAU_time_comm[MAX_MSG_SIZE];
char TAU_time_copy[MAX_MSG_SIZE];
char **TAU_time_steps;

char TAU_time_calu[MAX_MSG_SIZE];
char TAU_time_exec_tm[MAX_MSG_SIZE];
char TAU_time_read[MAX_MSG_SIZE];
char TAU_time_write[MAX_MSG_SIZE];

#endif /* _TAU_ */

void init_timers(int size);
void free_timers(int size);

double time_exec;
double time_tslu;
double time_mult;
double time_solv;
double time_inv;
double time_add;
double time_swap;
double time_rest;
double time_comm;
double time_copy;
double time_step;
double time_exec_final;
double time_tslu_final;
double time_mult_final;
double time_solv_final;
double time_inv_final;
double time_add_final;
double time_swap_final;
double time_rest_final;
double time_comm_final;
double time_copy_final;
double *time_steps;

double time_calu;
double time_exec_tm;
double time_read;
double time_write;
double time_calu_final;
double time_exec_tm_final;
double time_read_final;
double time_write_final;

#endif /* _TIMERS_H_ */

