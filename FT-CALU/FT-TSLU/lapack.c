#include "lapack.h"

void lu_matrix(double *A,int M,int N,int *IPIV){
	int LDA=M;
	int INFO=0;
#ifdef _TAU_
	TAU_START(TAU_time_tslu);
#endif /* _TAU_ */
	time_tslu=MPI_Wtime();
	dgetrf_(&M,&N,A,&LDA,IPIV,&INFO);
	time_tslu_final+=MPI_Wtime()-time_tslu;
#ifdef _TAU_
	TAU_STOP(TAU_time_tslu);
#endif /* _TAU_ */
}

void mult_matrix(double *A,int MA,int NA,double *B,int MB,int NB,double *C,int index){
	char TRANSA='N';
	char TRANSB='N';
	double ALPHA=1.0;
	double BETA=1.0;
	int LDAA=MA;
	int LDAB=MB;
	int LDAC=MA;
#ifdef _TAU_
	TAU_START(TAU_time_mult);
#endif /* _TAU_ */
	time_mult=MPI_Wtime();
	dgemm_(&TRANSA,&TRANSB,&MA,&NB,&NA,&ALPHA,A,&LDAA,B+index,&LDAB,&BETA,C,&LDAC);
	time_mult_final+=MPI_Wtime()-time_mult;
#ifdef _TAU_
	TAU_STOP(TAU_time_mult);
#endif /* _TAU_ */
}

void solve_matrix(double *A,double *U,int M,int N){
	char SIDE='R';
	char UPLO='U';
	char TRANSA='N';
	char DIAG='N';
	double ALPHA=1.0;
	int LDA=M;
	int LDB=M;
#ifdef _TAU_
	TAU_START(TAU_time_solv);
#endif /* _TAU_ */
	time_solv=MPI_Wtime();
	dtrsm_(&SIDE,&UPLO,&TRANSA,&DIAG,&M,&N,&ALPHA,U,&LDA,A,&LDB);
	time_solv_final+=MPI_Wtime()-time_solv;
#ifdef _TAU_
	TAU_STOP(TAU_time_solv);
#endif /* _TAU_ */
}

void inv_matrix(double *A,int M,int N,int *IPIV){
	int LDA=M;
	int INFO=0;
	int LWORK=N*N;
	double *WORK=(double *)malloc(LWORK*sizeof(double));
#ifdef _TAU_
	TAU_START(TAU_time_inv);
#endif /* _TAU_ */
	time_inv=MPI_Wtime();
	dgetri_(&N,A,&LDA,IPIV,WORK,&LWORK,&INFO);
	time_inv_final+=MPI_Wtime()-time_inv;
#ifdef _TAU_
	TAU_STOP(TAU_time_inv);
#endif /* _TAU_ */
	free(WORK);
}

void add_matrix(double *A,double *B,int M,int N,char op){
	double DA=1.0;
	int INCX=1;
	int INCY=1;
	int MN=M*N;
	int i,j;
#ifdef _TAU_
	TAU_START(TAU_time_add);
#endif /* _TAU_ */
	time_add=MPI_Wtime();
	if(op=='S'){
		for(i=0;i<M;i++)
			for(j=0;j<N;j++)
				B[j*M+i]*=-1.0;
	}
	daxpy_(&MN,&DA,A,&INCX,B,&INCY);
	time_add_final+=MPI_Wtime()-time_add;
#ifdef _TAU_
	TAU_STOP(TAU_time_add);
#endif /* _TAU_ */
}

void rand_matrix(double *A,int M,int N,int r){
	int seed[4]={0,0,0,2*r+1};
	int ONE=1;
	int MN=M*N;
	dlarnv_(&ONE,seed,&MN,A);
}

void swap_sub_block(double *A,int MA,int index,int M,int N,int *IPIV,int INCX){
	int LDA=M;
	int K1=1;
	int K2=min(M,N);
	double *sub=(double *)calloc(M*N,sizeof(double));
	get_sub_block(A,MA,index,sub,M,N);
#ifdef _TAU_
	TAU_START(TAU_time_swap);
#endif /* _TAU_ */
	time_swap=MPI_Wtime();
	dlaswp_(&N,sub,&LDA,&K1,&K2,IPIV,&INCX);
	time_swap_final+=MPI_Wtime()-time_swap;
#ifdef _TAU_
	TAU_STOP(TAU_time_swap);
#endif /* _TAU_ */
	set_sub_block(A,MA,index,sub,M,N);
	free(sub);
}

int random_number(){
	int i;
	int SIZE=100;
	int IDIST=1;
	int SEED_SIZE=4;
	int SEED[SEED_SIZE];
	int SEED_MAX=4096;
	int sum=0;
	for(i=0;i<SEED_SIZE;i++)
		SEED[i]=rand()%SEED_MAX;
	SEED[SEED_SIZE-1]=SEED[SEED_SIZE-1]+((SEED[SEED_SIZE-1]&1) ? 0 : 1);
	double *X=(double *)malloc(SIZE*sizeof(double));
	dlarnv_(&IDIST,SEED,&SIZE,X);
	for(i=0;i<SIZE;i++)
		sum+=(int)floor(X[i]*SIZE*SEED_MAX);
	free(X);
	return(sum%SIZE);
}

