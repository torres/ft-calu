#include "ft_tslu.h"

int ft_tslu_init(fttslu_data *ftd,double *A,int M,int N,int l_c,char *in,int argc,char **argv){
	ftd->md=matrix_data_init_with_matrix(M,N,M/ftd->mpid->world_size,N,A,1);
	if(ftd->md==NULL){
		fprintf(stderr,"Proceso [%d] en [%s]: Error al inicializar panel FT-TSLU\n",ftd->mpid->world_rank,ftd->mpid->processor_name);
		return(EXIT_FAILURE);
	}
	int ret=ft_tslu_spawned(ftd);
	if(ret!=MPI_SUCCESS){
		free_timers(ftd->mpid->world_size);
		return(EXIT_FAILURE);
	}
	return(MPI_SUCCESS);
}

int ft_tslu_spawned(fttslu_data *ftd){
	int ret;
	MPI_data *mpid=ftd->mpid;
#ifndef _OMPI_
	if(mpid->spawned){
#ifdef _TAU_
		TAU_START(TAU_time_rest);
#endif /* _TAU_ */
		time_rest=MPI_Wtime();
		ret=restore_data(ftd);
		time_rest_final+=MPI_Wtime()-time_rest;
#ifdef _TAU_
		TAU_STOP(TAU_time_rest);
#endif /* _TAU_ */
		if(ret!=MPI_SUCCESS){
			fprintf(stderr,"Proceso [%d] en [%s]: Error al restaurar datos\n",mpid->world_rank,mpid->processor_name);
			return(EXIT_FAILURE);
		}
	}
#endif /* _OMPI_ */
	return(MPI_SUCCESS);
}

int ft_tslu(fttslu_data *ftd,int l_c){
	MPI_data *mpid=ftd->mpid;
	matrix_data *md=ftd->md;
	init_variables(l_c,mpid->world_size);
#ifdef _TAU_
	TAU_START(TAU_time_exec);
#endif /* _TAU_ */
	time_exec=MPI_Wtime();
	matrix_data *md_sub,*md_subR;
	double *fakeblock;
	double *subblock;
	char err[MPI_MAX_ERROR_STRING];
	int index,indexD;
	int ret,err_len,i,j;
	int curr_step;
	int flag;
	int working_block[mpid->world_size];
	for(i=0;i<mpid->world_size;i++)
		working_block[i]=i;
#ifndef _OMPI_
	int indexS;
	int send_tries;
	int dead_list_size;
	int surv_list_size;
	int not_restored_size;
	int not_restored;
	int *dead_rank_list;
	int *surv_rank_list;
	int *notr_rank_list;
	int restored_send[mpid->world_size];
	int restored_recv[mpid->world_size];
	if(mpid->spawned){
#ifdef _TAU_
		TAU_START(TAU_time_rest);
#endif /* _TAU_ */
		time_rest=MPI_Wtime();
		curr_step=mpid->step;
		mpid->step=0;
		for(ret=0;ret<curr_step;ret++){
//			ACTUALIZA INDICES DE TRABAJO DE TODOS LOS PROCESOS
			for(i=0;i<mpid->world_size;i++)
				working_block[i]=min(working_block[i],working_block[mpid->dest[i]]);
//			ACTUALIZA DESTINOS DE TODOS LOS PROCESOS
			add_mirror(mpid);
			update_dest(mpid);
		}
		mpid->step=curr_step;
		mpid->spawned=!mpid->spawned;
		time_rest_final+=MPI_Wtime()-time_rest;
#ifdef _TAU_
		TAU_STOP(TAU_time_rest);
#endif /* _TAU_ */
	}
#endif /* _OMPI_ */
	while(mpid->step<step_lim-1){
//		PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO
		index=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->world_rank]);
//		PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO DESTINO
		indexD=get_block_index(md->Mb,md->Nb,md->M,md->N,working_block[mpid->dest[mpid->world_rank]]);
//		INTENTA	EJECUTAR UN PASO DEL ALGORITMO
		while(1){
//			printf("Proceso [%d] en [%s]: Ejecutando paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
			if(mpid->step_finished)
				ret=MPI_SUCCESS;
			else if(mpid->step==0){
				subblock=(double *)calloc(md->Mb*md->Nb,sizeof(double));
//				OBTENER EL PRIMER SUBBLOQUE A PARTIR DEL ELEMENTO index
				get_sub_block(md->A,md->M,index,subblock,md->Mb,md->Nb);
//				INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
				md_sub=matrix_data_init_with_matrix(md->Mb,md->Nb,0,0,subblock,0);
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
//					INICIALIZA EN CEROS UN SUBBLOQUE A RECIBIR
					fakeblock=(double *)calloc(md->Mb*md->Nb,sizeof(double));
					md_subR=matrix_data_init_with_matrix(md->Mb,md->Nb,0,0,fakeblock,0);
//					SIMULAR EL PRIMER PASO
					ret=MPI_SUCCESS;
				}
				else{
//					INICIALIZA EL SUBBLOQUE A RECIBIR
					md_subR=matrix_data_init(md->Mb,md->Nb,0,0,0);
//					EJECUTA EL PRIMER PASO
#ifdef _TAU_
					TAU_START(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
					time_step=MPI_Wtime();
					ret=first_middle_step(md_sub,md_subR,mpid);
					time_steps[mpid->step]+=MPI_Wtime()-time_step;
#ifdef _TAU_
					TAU_STOP(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
				}
			}
			else{
//				INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
				md_sub=matrix_data_init_with_matrix(MU,NU,0,0,concatU,0);
//				SI EL PRIMER ELEMENTO DEL SUBBLOQUE DEL PROCESO LOCAL Y DESTINO ES IGUAL
				if(index==indexD){
//					INICIALIZA EN CEROS UN SUBBLOQUE A RECIBIR
					fakeblock=(double *)calloc(MU*NU,sizeof(double));
					md_subR=matrix_data_init_with_matrix(MU,NU,0,0,fakeblock,0);
//					SIMULA EL PRIMER PASO
					ret=MPI_SUCCESS;
				}
				else{
//					INICIALIZA EL SUBBLOQUE A RECIBIR
					md_subR=matrix_data_init(MU,NU,0,0,0);
//					EJECUTA UN PASO INTERMEDIO
#ifdef _TAU_
					TAU_START(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
					time_step=MPI_Wtime();
					ret=first_middle_step(md_sub,md_subR,mpid);
					time_steps[mpid->step]+=MPI_Wtime()-time_step;
#ifdef _TAU_
					TAU_STOP(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
				}
			}
			mpid->step_finished=flag=(ret==MPI_SUCCESS);
#ifndef _OMPI_
			MPIX_Comm_agree(mpid->world,&flag);
#else
			if(!flag){
				fprintf(stderr,"Proceso [%d] en [%s]: Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
				return(ret);
			}
			MPI_Barrier(mpid->world);
#endif /* _OMPI_ */
//			SI NO OCURRIO ERROR, TERMINA EL PASO DEL ALGORITMO
			if(flag){
//				printf("Proceso [%d] en [%s]: Ejecutado paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
				backup_results(md,md_sub,index,md_subR,indexD,(mpid->world_rank<=mpid->dest[mpid->world_rank]) ? 'A' : 'B');
				concatenate_matrices(md_sub,md_subR,mpid->step,(mpid->world_rank<=mpid->dest[mpid->world_rank]) ? 'A' : 'B');
//				LIBERA RECURSOS
				matrix_data_free(md_subR);
				break;
			}
#ifndef _OMPI_
#ifdef _TAU_
			TAU_START(TAU_time_rest);
#endif /* _TAU_ */
			time_rest=MPI_Wtime();
			fprintf(stderr,"Proceso [%d] en [%s]: Error detectado en paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
			while(1){
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS FALLIDOS
				dead_rank_list=dead_list(mpid,&dead_list_size);
//				CADA PROCESO OBTIENE UNA LISTA DE PROCESOS SOBREVIVIENTES
				surv_rank_list=survivor_list(mpid,dead_rank_list,dead_list_size,&surv_list_size);
/*				sprintf(msg,"Proceso [%d] en [%s]: Procesos fallidos: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<dead_list_size;i++)
					sprintf(msg,"%s[%d]",msg,dead_rank_list[i]);
				printf("%s\n",msg);
				sprintf(msg,"Proceso [%d] en [%s]: Procesos sobrevivientes: ",mpid->world_rank,mpid->processor_name);
				for(i=0;i<surv_list_size;i++)
					sprintf(msg,"%s[%d]",msg,surv_rank_list[i]);
				printf("%s\n",msg);*/
//				RESTAURA A LOS PROCESOS FALLIDOS
				ret=replace_partners(mpid,dead_rank_list,dead_list_size);
//				SI LA RESTAURACION FALLO, INTENTA DE NUEVO
				if(ret!=MPI_SUCCESS){
					free(dead_rank_list);
					free(surv_rank_list);
					MPI_Error_string(ret,err,&err_len);
					fprintf(stderr,"Proceso [%d] en [%s]: Error al restaurar procesos [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
					continue;
				}
				break;
			}
//			SE BUSCA LA POSICION EN LA LISTA DE SOBREVIVIENTES
			indexS=0;
			while(mpid->world_rank!=surv_rank_list[indexS]){
				indexS++;
			}
//			SI EL ERROR OCURRIO EN EL PRIMER PASO
			if(mpid->step==0){
//				SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO FALLIDO
				if(indexS<dead_list_size){
					for(i=indexS;i<dead_list_size;i+=surv_list_size){
						printf("Proceso [%d] en [%s]: Enviando paso inicial a [%d]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
//						INTENTA ENVIAR EL PASO DE INICIO
						ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//						SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
						if(ret!=MPI_SUCCESS){
							MPI_Error_string(ret,err,&err_len);
							fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
							continue;
						}
						printf("Proceso [%d] en [%s]: Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
					}
				}
			}
//			SI EL ERROR OCURRIO EN UN PASO INTERMEDIO
			else{
//				INICIALIZA EN 0 LA LISTA DE PROCESOS RESTAURADOS POR EL PROCESO LOCAL
				memset(restored_send,0,mpid->world_size*sizeof(int));
				if(mpid->step_finished){
					concat_sizes[MU_POS]=MU_prev;
					concat_sizes[NU_POS]=NU_prev;
					concatU_ptr=concatU_prev;
				}
				else{
					concat_sizes[MU_POS]=MU;
					concat_sizes[NU_POS]=NU;
					concatU_ptr=concatU;
				}
//				BUSCA SI ALGUN PROCESO FALLIDO ES AQUEL CON EL QUE HE COMPARTIDO DATOS
				for(i=0;i<dead_list_size;i++){
					for(j=0;j<mpid->step;j++){
//						SI HE COMPARTIDO DATOS CON EL PROCESO FALLIDO, LE ENVIO MIS DATOS ACTUALES
						if(dead_rank_list[i]==mpid->mirror[j]){
							printf("Proceso [%d] en [%s]: Enviando datos a [%d]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
//							INTENTA ENVIAR EL PASO DE INICIO
							ret=safe_send_times((void *)(&(mpid->step)),1,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								continue;
							}
//							INTENTA ENVIAR LA MATRIZ A EN EL PASO ACTUAL
							ret=safe_send_times((void *)md->A,md->M*md->N,MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir matriz A a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
//							INTENTA ENVIAR LAS DIMENSIONES DE LAS CONCATENACIONES EN EL PASO ACTUAL
							ret=safe_send_times((void *)concat_sizes,MAX_CONCAT_SIZES,MPI_INT,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir dimensiones de concatenaciones a [%d] [%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
//							INTENTA ENVIAR LA CONCATENACION DE MATRICES U EN EL PASO ACTUAL
							ret=safe_send_times((void *)concatU_ptr,concat_sizes[MU_POS]*concat_sizes[NU_POS],MPI_DOUBLE,dead_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir concatenacion U a [%d] [%d][%s]\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i],ret,err);
								break;
							}
							restored_send[dead_rank_list[i]]=1;
							printf("Proceso [%d] en [%s]: Proceso [%d] restaurado\n",mpid->world_rank,mpid->processor_name,dead_rank_list[i]);
							break;
						}
					}
				}
//				INFORMA A LOS PROCESOS SOBREVIVIENTES LOS PROCESOS QUE FUERON RESTAURADOS
				for(i=0;i<surv_list_size;i++){
					if(mpid->world_rank==surv_rank_list[i])
						continue;
					send_tries=MAX_TRY;
					while(1){
//						SI LOS INTENTOS DE ENVIO FALLARON, CONTINUA CON EL PROXIMO PROCESO
						if(send_tries==0){
							fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir lista de procesos restaurados a [%d]. Abortando [%d][%s]\n",mpid->world_rank,mpid->processor_name,surv_rank_list[i],ret,err);
							break;
						}
#ifdef _TAU_
						TAU_START(TAU_time_comm);
#endif /* _TAU_ */
						time_comm=MPI_Wtime();
						ret=MPI_Send(restored_send,mpid->world_size,MPI_INT,surv_rank_list[i],0,mpid->world);
						time_comm_final+=MPI_Wtime()-time_comm;
#ifdef _TAU_
						TAU_STOP(TAU_time_comm);
#endif /* _TAU_ */
//						SI EL ENVIO FALLO, INTENTA DE NUEVO
						if(ret!=MPI_SUCCESS){
							MPI_Error_string(ret,err,&err_len);
							fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir lista de procesos restaurados a [%d]. Reintentando [%d][%s]\n",mpid->world_rank,mpid->processor_name,surv_rank_list[i],ret,err);
							send_tries--;
							continue;
						}
						break;
					}
				}
//				RECIBE INFORMES DE LOS PROCESOS SOBREVIVIENTES SOBRE LOS PROCESOS QUE FUERON RESTAURADOS
				for(i=0;i<surv_list_size;i++){
					if(mpid->world_rank==surv_rank_list[i])
						continue;
					send_tries=MAX_TRY;
					while(1){
//						SI LOS INTENTOS DE ENVIO FALLARON, CONTINUA CON EL PROXIMO PROCESO
						if(send_tries==0){
							fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir lista de procesos restaurados de [%d]. Abortando [%d][%s]\n",mpid->world_rank,mpid->processor_name,surv_rank_list[i],ret,err);
							break;
						}
						memset(restored_recv,0,mpid->world_size*sizeof(int));
#ifdef _TAU_
						TAU_START(TAU_time_comm);
#endif /* _TAU_ */
						time_comm=MPI_Wtime();
						ret=MPI_Recv(restored_recv,mpid->world_size,MPI_INT,surv_rank_list[i],0,mpid->world,&(mpid->status));
						time_comm_final+=MPI_Wtime()-time_comm;
#ifdef _TAU_
						TAU_STOP(TAU_time_comm);
#endif /* _TAU_ */
//						SI EL ENVIO FALLO, INTENTA DE NUEVO
						if(ret!=MPI_SUCCESS){
							MPI_Error_string(ret,err,&err_len);
							fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir lista de procesos restaurados de [%d]. Reintentando [%d][%s]\n",mpid->world_rank,mpid->processor_name,surv_rank_list[i],ret,err);
							send_tries--;
							continue;
						}
						break;
					}
					if(ret==MPI_SUCCESS){
//						ACTUALIZA LISTA DE PROCESOS RESTAURADOS
						for(j=0;j<mpid->world_size;j++)
							restored_send[j]=(restored_send[j]|restored_recv[j]);
					}
				}
//				BUSCA LOS PROCESOS QUE NO FUERON RESTAURADOS
				not_restored_size=0;
				for(i=0;i<dead_list_size;i++)
					not_restored_size+=(restored_send[dead_rank_list[i]]==0) ? 1 : 0;
				if(not_restored_size>0){
					notr_rank_list=(int *)calloc(not_restored_size,sizeof(int));
					not_restored=0;
					for(i=0;i<dead_list_size;i++){
						if(restored_send[dead_rank_list[i]]==0){
							notr_rank_list[not_restored]=dead_rank_list[i];
							not_restored++;
						}
					}
/*					printf("Proceso [%d] en [%s]: notr_rank_list: ",mpid->world_rank,mpid->processor_name);
					for(i=0;i<not_restored_size;i++)
						printf("[%d]",notr_rank_list[i]);
					printf("\n");*/
//					SI ME CORRESPONDE ENVIAR DATOS A UN PROCESO NO RESTAURADO
					if(indexS<not_restored_size){
						curr_step=0;
						for(i=indexS;i<not_restored_size;i+=surv_list_size){
							printf("Proceso [%d] en [%s]: Enviando paso inicial a [%d]\n",mpid->world_rank,mpid->processor_name,notr_rank_list[i]);
//							INTENTA ENVIAR EL PASO DE INICIO
							ret=safe_send_times((void *)(&curr_step),1,MPI_INT,notr_rank_list[i],mpid->world,MAX_TRY);
//							SI EL INTENTO DE ENVIO FALLO, CONTINUA CON EL PROXIMO PROCESO
							if(ret!=MPI_SUCCESS){
								MPI_Error_string(ret,err,&err_len);
								fprintf(stderr,"Proceso [%d] en [%s]: Error al distribuir paso inicial a [%d][%d][%s]\n",mpid->world_rank,mpid->processor_name,notr_rank_list[i],ret,err);
								continue;
							}
							printf("Proceso [%d] en [%s]: Proceso [%d] restaurado desde el inicio\n",mpid->world_rank,mpid->processor_name,notr_rank_list[i]);
						}
					}
					free(notr_rank_list);
				}
			}
			free(surv_rank_list);
			free(dead_rank_list);
			time_rest_final+=MPI_Wtime()-time_rest;
#ifdef _TAU_
			TAU_STOP(TAU_time_rest);
#endif /* _TAU_ */
#endif /* _OMPI_ */
		}
//		ACTUALIZA INDICES DE TRABAJO DE TODOS LOS PROCESOS
		for(i=0;i<mpid->world_size;i++)
			working_block[i]=min(working_block[i],working_block[mpid->dest[i]]);
//		ACTUALIZA DESTINOS DE TODOS LOS PROCESOS
		add_mirror(mpid);
		update_dest(mpid);
		mpid->step_finished=0;
#ifndef _OMPI_
		MU_prev=MU;
		NU_prev=NU;
		if(concatU_prev!=NULL)
			free(concatU_prev);
		concatU_prev=(double *)calloc(MU_prev*NU_prev,sizeof(double));
#ifdef _TAU_
		TAU_START(TAU_time_copy);
#endif /* _TAU_ */
		time_copy=MPI_Wtime();
		memcpy(concatU_prev,concatU,MU_prev*NU_prev*sizeof(double));
		time_copy_final+=MPI_Wtime()-time_copy;
#ifdef _TAU_
		TAU_STOP(TAU_time_copy);
#endif /* _TAU_ */
#endif /* _OMPI_ */
	}
//	printf("Proceso [%d] en [%s]: Ejecutando ultimo paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
//	INICIALIZA EL SUBBLOQUE A PROCESAR LOCALMENTE
	md_sub=matrix_data_init_with_matrix(MU,NU,0,0,concatU,0);
#ifdef _TAU_
	TAU_START(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
	time_step=MPI_Wtime();
	ret=last_step(md_sub);
	time_steps[mpid->step]+=MPI_Wtime()-time_step;
#ifdef _TAU_
	TAU_STOP(TAU_time_steps[mpid->step]);
#endif /* _TAU_ */
//	printf("Proceso [%d] en [%s]: Ejecutado ultimo paso [%d]\n",mpid->world_rank,mpid->processor_name,mpid->step);
//	ASIGNA LOS BLOQUES FINALES
	set_U(md_sub);
	double *tmp=md->U;
	md->U=md_sub->U;
	md_sub->U=tmp;
//	set_sub_block(md->U,min(md->M,md->N),0,md_sub->U,min(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N));
	if(l_calc){
		double *new_U=pad_U(md->U,min(md->M,md->N),md->M,md->N);
		solve_matrix(md->A_init,new_U,md->M,md->N);
		lu_matrix(md->A_init,md->M,md->N,md->IPIV);//TOMA MUCHO TIEMPO APLICAR LU PARA REORDENAR
		free(new_U);
//		RESTABLECE EL BLOQUE A PARTIR DE L Y U
		set_A(md,1);
	}
	else{
//		RESTABLECE EL BLOQUE A PARTIR DE L Y U
		set_A(md,0);
	}
//	LIBERA RECURSOS
	matrix_data_free(md_sub);
	time_exec_final+=MPI_Wtime()-time_exec;
#ifdef _TAU_
	TAU_STOP(TAU_time_exec);
#endif /* _TAU_ */
	return(ret);
}

int restore_data(fttslu_data *ftd){
	MPI_data *mpid=ftd->mpid;
	matrix_data *md=ftd->md;
	char err[MPI_MAX_ERROR_STRING];
	int err_len;
	int ret;
//	RECIBO PASO DE INICIO
	ret=safe_receive_times((void *)(&(mpid->step)),1,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//	SI OCURRIO ALGUN ERROR EN LA RECEPCION
	if(ret!=MPI_SUCCESS){
		MPI_Error_string(ret,err,&err_len);
		fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir el paso inicial [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
		return(ret);
	}
	if(mpid->step>0){
//		RECIBO ESTADO ACTUAL DE LA MATRIZ A
		ret=safe_receive_times((void *)md->A,md->M*md->N,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir matriz A [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		set_U(md);
//		RECIBO DIMENSIONES DE LAS CONCATENACIONES
		ret=safe_receive_times((void *)concat_sizes,MAX_CONCAT_SIZES,MPI_INT,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir dimensiones de concatenaciones [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			return(ret);
		}
		MU=concat_sizes[MU_POS];
		NU=concat_sizes[NU_POS];
		concatU=(double *)calloc(MU*NU,sizeof(double));
//		RECIBO ESTADO ACTUAL DE LA CONCATENACION U
		ret=safe_receive_times((void *)concatU,MU*NU,MPI_DOUBLE,MPI_ANY_SOURCE,mpid->world,mpid->status,MAX_TRY);
//		SI OCURRIO ALGUN ERROR EN LA RECEPCION
		if(ret!=MPI_SUCCESS){
			MPI_Error_string(ret,err,&err_len);
			fprintf(stderr,"Proceso [%d] en [%s]: Error al recibir concatenacion U [%d][%s]\n",mpid->world_rank,mpid->processor_name,ret,err);
			free(concatU);
			return(ret);
		}
	}
	return(MPI_SUCCESS);
}

int first_middle_step(matrix_data *md_sub,matrix_data *md_subR,MPI_data *mpid){
//	EJECUTA DGETRF SOBRE EL SUBBLOQUE
	lu_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->IPIV);
//	ENVIA SUBBLOQUE RESULTANTE Y RECIBE SUBBLOQUE CALCULADO POR OTRO PROCESO
	return(send_receive(md_sub->A,md_subR->A,md_sub->M,md_sub->N,md_sub->IPIV,md_subR->IPIV,mpid));
}

int last_step(matrix_data *md_sub){
//	EJECUTA DGETRF SOBRE EL SUBBLOQUE
	lu_matrix(md_sub->A,md_sub->M,md_sub->N,md_sub->IPIV);
	return(MPI_SUCCESS);
}

void backup_results(matrix_data *md,matrix_data *md_sub,int index,matrix_data *md_subR,int indexD,char up){
//	RESPALDA LOS RESULTADOS DE A
	set_sub_block(md->A,md->M,index,md_sub->A,md_sub->M,md_sub->N);
	if(index!=indexD)
		set_sub_block(md->A,md->M,indexD,md_subR->A,md_subR->M,md_subR->N);
//	RESPALDA EL RESULTADO DE U DEL PROCESO CON MENOR RANK
	set_U(md_sub);
	set_U(md_subR);
	if(up=='A')
		set_sub_block(md->U,min(md->M,md->N),0,md_sub->U,min(md_sub->M,md_sub->N),min(md_sub->M,md_sub->N));
	else
		set_sub_block(md->U,min(md->M,md->N),0,md_subR->U,min(md_subR->M,md_subR->N),min(md_subR->M,md_subR->N));
}

void concatenate_matrices(matrix_data *md_sub,matrix_data *md_subR,int step,char up){
	if(concatU!=NULL)
		free(concatU);
	if(step==0)
		concatU=concat_U_blocks_first_step(md_sub->A,md_sub->M,md_sub->N,md_subR->A,md_subR->M,md_subR->N,up);
	else
		concatU=concat_U_blocks(md_sub->U,min(md_sub->M,md_sub->N),md_subR->U,min(md_subR->M,md_subR->N),up);
	MU=min(md_sub->M,md_sub->N)+min(md_subR->M,md_subR->N);
	NU=min(md_sub->M,md_sub->N);
}

void free_fttslu_data(fttslu_data *ftd){
	if(ftd->md!=NULL)
		matrix_data_free(ftd->md);
	if(ftd->mpid!=NULL)
		MPI_data_free(ftd->mpid);
	if(ftd!=NULL)
		free(ftd);
}

void init_variables(int l_c,int world_size){
	l_calc=l_c;
	step_lim=((int)ceil(log2(world_size)))+1;
}

