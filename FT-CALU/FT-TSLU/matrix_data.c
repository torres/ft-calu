#include "matrix_data.h"

matrix_data *matrix_data_init_with_random(int M,int N,int Mb,int Nb,int rank,int copy){
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy);
	rand_matrix(m->A,M,N,rank);
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_file(int M,int N,int Mb,int Nb,char *file,int copy){
	FILE *in=fopen(file,"r");
	if(in==NULL)
		return(NULL);
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy);
	int i,j;
	for(i=0;i<m->M;i++)
		for(j=0;j<m->N;j++)
			fscanf(in,"%lf",&(m->A[j*m->M+i]));
	fclose(in);
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init_with_matrix(int M,int N,int Mb,int Nb,double *A,int copy){
	matrix_data *m=matrix_data_init(M,N,Mb,Nb,copy);
	free(m->A);
	m->A=A;
	if(copy)
		set_A_init(m);
	return(m);
}

matrix_data *matrix_data_init(int M,int N,int Mb,int Nb,int copy){
	matrix_data *m=(matrix_data *)calloc(1,sizeof(matrix_data));
	m->M=M;
	m->N=N;
	m->A=(double *)calloc(m->M*m->N,sizeof(double));
	if(copy)
		m->A_init=(double *)calloc(m->M*m->N,sizeof(double));
	m->LDA=m->M;
	m->IPIV=(int *)calloc(min(m->M,m->N),sizeof(int));
	m->INFO=0;
	m->U=(double *)calloc(min(m->M,m->N)*min(m->M,m->N),sizeof(double));
	m->Mb=Mb;
	m->Nb=Nb;
	return(m);
}

void matrix_data_free(matrix_data *md){
	if(md->U!=NULL)
		free(md->U);
	if(md->IPIV!=NULL)
		free(md->IPIV);
	if(md->A_init!=NULL)
		free(md->A_init);
	if(md->A!=NULL)
		free(md->A);
	if(md!=NULL)
		free(md);
}

void set_U(matrix_data *md){
	int i,j,min_md=min(md->M,md->N);
#ifdef _TAU_
	TAU_START(TAU_time_copy);
#endif /* _TAU_ */
	time_copy=MPI_Wtime();
	for(i=0;i<min_md;i++)
		for(j=0;j<min_md;j++)
			md->U[j*min_md+i]=(j>=i) ? md->A[j*md->M+i] : 0;
	time_copy_final+=MPI_Wtime()-time_copy;
#ifdef _TAU_
	TAU_STOP(TAU_time_copy);
#endif /* _TAU_ */
}

void set_A(matrix_data *md,int src){
	int i,j,min_md=min(md->M,md->N);
#ifdef _TAU_
	TAU_START(TAU_time_copy);
#endif /* _TAU_ */
	time_copy=MPI_Wtime();
	for(i=0;i<md->M;i++)
		for(j=0;j<md->N;j++)
			md->A[j*md->M+i]=(j<i && src==0) ? 0 : (j<i && src==1) ? md->A_init[j*md->M+i] : md->U[j*min_md+i];
	time_copy_final+=MPI_Wtime()-time_copy;
#ifdef _TAU_
	TAU_STOP(TAU_time_copy);
#endif /* _TAU_ */
}

void set_A_init(matrix_data *md){
#ifdef _TAU_
	TAU_START(TAU_time_copy);
#endif /* _TAU_ */
	time_copy=MPI_Wtime();
	memcpy(md->A_init,md->A,md->M*md->N*sizeof(double));
	time_copy_final+=MPI_Wtime()-time_copy;
#ifdef _TAU_
	TAU_STOP(TAU_time_copy);
#endif /* _TAU_ */
}

void print_matrix_data(matrix_data *md){
	int i,j,min_md=min(md->M,md->N);
	for(i=0;i<md->M;i++){
		for(j=0;j<md->N;j++){
			if(md->A[j*md->M+i]>=0.0)
				printf("+");
			printf("%.4lf ",md->A[j*md->M+i]);
		}
		if(i<min_md){
			printf("\t|\t");
			for(j=0;j<min_md;j++){
				if(md->U[j*min_md+i]>=0.0)
					printf("+");
				printf("%.4lf ",md->U[j*min_md+i]);
			}
		}
		printf("\n");
	}
}

