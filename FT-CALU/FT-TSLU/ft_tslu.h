#ifndef _FT_TSLU_H_
#define _FT_TSLU_H_

#include "lapack.h"
#include "mpi_data.h"
#include "matrix_data.h"

#define WAIT_TIME			10000
#define MU_POS				0
#define NU_POS				1
#define MAX_CONCAT_SIZES	2

typedef struct _fttslu_data{
	matrix_data *md;
	MPI_data *mpid;
}fttslu_data;

int ft_tslu_init(fttslu_data *ftd,double *A,int M,int N,int l_c,char *in,int argc,char **argv);
int ft_tslu_spawned(fttslu_data *ftd);
int ft_tslu(fttslu_data *ftd,int l_c);
int restore_data(fttslu_data *ftd);
int first_middle_step(matrix_data *md_sub,matrix_data *md_subR,MPI_data *mpid);
int last_step(matrix_data *md_sub);

void backup_results(matrix_data *md,matrix_data *md_sub,int index,matrix_data *md_subR,int indexD,char up);
void concatenate_matrices(matrix_data *md_sub,matrix_data *md_subR,int step,char up);
void free_fttslu_data(fttslu_data *ftd);
void init_variables(int l_c,int world_size);

int concat_sizes[MAX_CONCAT_SIZES];
int l_calc;
int step_lim;
int MU,NU;
double *concatU;

#ifndef _OMPI_
int MU_prev,NU_prev;
double *concatU_prev;
double *concatU_ptr;
#endif /* _OMPI_ */

#endif /* _FT_TSLU_H_ */

