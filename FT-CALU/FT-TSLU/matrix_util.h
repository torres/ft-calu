#ifndef _MATRIX_UTIL_H_
#define _MATRIX_UTIL_H_

#include "util.h"

double *concat_U_blocks_first_step(double *A,int MA,int NA,double *B,int MB,int NB,char up);
double *concat_U_blocks(double *A,int NA,double *B,int NB,char up);
double *concat_U_blocks_horizontal(double *A,int MA,int NA,double *B,int MB,int NB,char l);
double *pack_U(double *U,int M,int N);
double *unpack_U(double *U,int N);
double *pad_U(double *U,int MU,int M,int N);

void get_sub_block(double *A,int MA,int index,double *sub,int M,int N);
void set_sub_block(double *A,int MA,int index,double *sub,int M,int N);
void print_matrix(double *A,int M,int N);
void print_IPIV(int *IPIV,int n);

#endif /* _MATRIX_UTIL_H_ */

