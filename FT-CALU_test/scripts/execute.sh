#! /bin/bash

if [ "$#" -ne 18 ]; then
	echo "Uso: execute.sh -f [inputfile | random] -M [M] -N [N] -L [true | false] -P [max_process] -c [OMPI | ULFM] -o [true | false] -T [true | false] -V [true | false]"
	exit 1
fi

while getopts f:M:N:L:p:P:c:o:T:V: option
do
	case "${option}"
	in
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
		P) MAX_PROCESS=${OPTARG};;
		c) COMPILE=${OPTARG};;
		o) OVER=${OPTARG};;
		T) TAU_U=${OPTARG};;
		V) VERB=${OPTARG};;
	esac
done

#--------------------------------------------------PREPARE ARGUMENTS AND OPTIONS
if [ "$LCALC" = "true" ]; then
	L="1"
	LU="LU"
else
	L="0"
	LU="U"
fi

if [ "$TAU_U" = "true" ]; then
	TAU_COMP="TAU_EXEC=-D_TAU_"
	TAU_USAGE="tau_exec -T mpi"
	export TAU_TRACE=1
else
	TAU_COMP=""
	TAU_USAGE=""
	export TAU_TRACE=0
fi

if [ "$VERB" = "true" ]; then
	VERBOSE="VERB=-D_VERB_"
else
	VERBOSE=""
fi

if [ "$OVER" = "true" ]; then
	OVERSUBSCRIBE="--map-by :oversubscribe"
else
	OVERSUBSCRIBE=""
fi

EXTRA_FLAGS="--map-by node --mca btl tcp,vader,self"
if [ "$COMPILE" = "OMPI" ]; then
	OMPI_ULFM="OMPI=-D_OMPI_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE}"
else
	OMPI_ULFM="OMPI=-D_ULFM_"
	EXTRA_FLAGS="${EXTRA_FLAGS} ${OVERSUBSCRIBE} --with-ft mpi --enable-recovery --mca mpi_ft_detector_thread true"
fi

MAKE="${TAU_COMP} ${OMPI_ULFM} ${VERBOSE}"
#--------------------------------------------------GENERATE INCLUDE AND LIBRARY DIRECTORIES IF NECCESARY
cd ..
INC_DIR=$PWD/FT-CALU/include
LIB_DIR=$PWD/FT-CALU/libs
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-CALU/FT-TSLU
#--------------------------------------------------COMPILE ACCORDING TO USER'S SELECTION
	make $MAKE
	make clean
	cd ..
	make $MAKE
	make clean
	cd ..
fi
cd FT-CALU_test/
make $MAKE

#--------------------------------------------------EXECUTE
MPI_EXEC="mpiexec"
N_PROCESS="--np ${MAX_PROCESS}"
HOSTFILE="--machinefile hostfile"
#HOSTFILE="--machinefile $OAR_FILE_NODES"
EXECUTABLE=ft_calu_test
ARGS="${M} ${N} ${L} ${INPUT_FILE}"

echo "********************************************************************"
time $MPI_EXEC $N_PROCESS $HOSTFILE $EXTRA_FLAGS $TAU_USAGE ./$EXECUTABLE $ARGS
echo "********************************************************************"

#--------------------------------------------------PREPARE RESULTS AND FINISH
RES_DIR=$PWD/results
if [ ! -z "$TAU_COMP" ]; then
	FILE_RES="profile.* events.*edf tautrace.*trc tau.edf result_${M}x${N}.* *.res spawn*"
	paraprof --pack result_${M}x${N}.ppk
	tau_merge -e events.*edf tautrace.*trc result_${M}x${N}.trc
	tau2slog2 result_${M}x${N}.trc tau.edf -o result_${M}x${N}.slog2
else
	FILE_RES="*.res"
fi

mkdir -p $RES_DIR/"[FTCALU][${COMPILE}][${LU}][${M}x${N}][${MAX_PROCESS}]_Processes"
mv $FILE_RES $RES_DIR/"[FTCALU][${COMPILE}][${LU}][${M}x${N}][${MAX_PROCESS}]_Processes"/

make clean

