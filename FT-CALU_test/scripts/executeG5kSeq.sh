#! /bin/bash

if [ "$#" -ne 10 ]; then
	echo "Uso: executeSeq.sh -t [test_times] -f [inputfile | random] -M [M] -N [N] -L [true | false]"
	exit 1
fi

while getopts t:f:M:N:L: option
do
	case "${option}"
	in
		t) TEST_TIMES=${OPTARG};;
		f) INPUT_FILE=${OPTARG};;
		M) M=${OPTARG};;
		N) N=${OPTARG};;
		L) LCALC=${OPTARG};;
	esac
done

#--------------------------------------------------PREPARE ARGUMENTS AND OPTIONS
if [ "$LCALC" = "true" ]; then
	L="1"
	LU="LU"
else
	L="0"
	LU="U"
fi

#--------------------------------------------------EXECUTE
EXECUTABLE=calu_seq_test
ARGS="${M} ${N} ${L} ${INPUT_FILE}"

cd ..
INC_DIR=$PWD/FT-CALU/include
LIB_DIR=$PWD/FT-CALU/libs
if [ ! -d "$INC_DIR" ] || [ ! -d "$LIB_DIR" ]; then
	cd FT-CALU/FT-TSLU
	make
	make clean
	cd ..
	make
	make clean
	cd ..
fi
cd FT-CALU_test/

make

RES_DIR=$PWD/results
FILE_RES="*.res"

echo "********************************************************************"
for (( i = 0; i < TEST_TIMES; i++ )); do
	time ./$EXECUTABLE $ARGS
	echo "********************************************************************"
	mkdir -p $RES_DIR/"[DGETRF][SEQ][${LU}][${M}x${N}][1]_Processes_execution[${i}]"
	mv $FILE_RES $RES_DIR/"[DGETRF][SEQ][${LU}][${M}x${N}][1]_Processes_execution[${i}]"/
done

make clean

