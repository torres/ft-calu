#include "ft_calu.h"

void write_times(matrix_data *md);

int main(int argc,char *argv[]){
	if(argc!=5){
		fprintf(stderr,"Uso: %s M N l_calc input_file\n",argv[0]);
		return(EXIT_FAILURE);
	}
	fttslu_data *ftd=(fttslu_data *)malloc(sizeof(fttslu_data));

	int M=atoi(argv[1]);
	int N=atoi(argv[2]);
	int l_c=(atoi(argv[3])!=0);
	char in[strlen(argv[4])];
	memcpy(in,argv[4],strlen(argv[4])*sizeof(char));
	if(strcmp(in,"random")==0)
		ftd->md=matrix_data_init_with_random(M,N,M,N,0,0);
	else
		ftd->md=matrix_data_init_with_file(M,N,M,N,in,0);
	if(ftd->md==NULL){
		fprintf(stderr,"Error al inicializar datos\n");
		free_fttslu_data(ftd);
		return(EXIT_FAILURE);
	}
	init_timers(1);
	time_exec=MPI_Wtime();
	lu_matrix(ftd->md->A,M,N,ftd->md->IPIV);
	set_U(ftd->md);
	time_exec_final+=MPI_Wtime()-time_exec;
//	ESCRIBE ARCHIVOS DE RESULTADO
	write_times(ftd->md);
	matrix_data_free(ftd->md);
	free(ftd);
	return(EXIT_SUCCESS);
}

void write_times(matrix_data *md){
	int i;
	char filename[MAX_MSG_SIZE];
	char compilation[5];
	char l_c[3];
#ifdef _OMPI_
	sprintf(compilation,"%s","OMPI");
#else
	sprintf(compilation,"%s","ULFM");
#endif /* _OMPI_ */
	sprintf(l_c,"%s",(l_calc) ? "LU" : "U");
	sprintf(filename,"[%s][%s][%s][%dx%d][%d]_Process[%d]_Times.res","DGETRF","SEQ",l_c,md->M,md->N,1,0);
	FILE *out=fopen(filename,"w");
	if(out==NULL)
		fprintf(stderr,"Error al crear el archivo [%s]\n",filename);
	else{
		fprintf(out,"exec: [%.6f] seg.\n",time_exec_final);
		fprintf(out,"tslu: [%.6f] seg.\n",time_tslu_final);
		fprintf(out,"mult: [%.6f] seg.\n",time_mult_final);
		fprintf(out,"solv: [%.6f] seg.\n",time_solv_final);
		fprintf(out,"inv: [%.6f] seg.\n",time_inv_final);
		fprintf(out,"add: [%.6f] seg.\n",time_add_final);
		fprintf(out,"swap: [%.6f] seg.\n",time_swap_final);
		fprintf(out,"rest: [%.6f] seg.\n",time_rest_final);
		fprintf(out,"comm: [%.6f] seg.\n",time_comm_final);
		fprintf(out,"copy: [%.6f] seg.\n",time_copy_final);
		for(i=0;i<step_lim;i++)
			fprintf(out,"step[%d]: [%.6f] seg.\n",i,time_steps[i]);
		fprintf(out,"calu: [%.6f] seg.\n",time_calu_final);
		fprintf(out,"exec_tm: [%.6f] seg.\n",time_exec_tm_final);
		fprintf(out,"read: [%.6f] seg.\n",time_read_final);
		fprintf(out,"write: [%.6f] seg.\n",time_write_final);
		fclose(out);
	}
}

